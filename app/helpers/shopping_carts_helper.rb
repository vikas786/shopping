module ShoppingCartsHelper

  def total_price_product(product, quantity)
    (product.try(:[], :price) || 0 ).to_i * quantity
  end

end
