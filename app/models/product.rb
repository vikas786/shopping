class Product < ActiveRecord::Base

  has_and_belongs_to_many :shopping_carts
  validates_presence_of :name, :item_type, :price
  scope :search, -> (search_term) { where('name ILIKE :query OR item_type ILIKE :query OR price ILIKE :query OR brand_name ILIKE :query', query: "%#{search_term}%") }
  validates :price, format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }

end
