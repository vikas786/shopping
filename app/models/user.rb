class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  belongs_to  :role
  has_one :shopping_cart

  after_create  :create_shopping_cart_for_user

  def create_shopping_cart_for_user
    ShoppingCart.create!(user_id: self.id)
  end

end
