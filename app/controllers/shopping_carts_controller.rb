class ShoppingCartsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_cart
  def index
    @products = @cart.products.group_by(&:name)
  end

  def create
    products = @cart.products << Product.find(params[:product_id]) rescue []
    if products.present?
      flash[:success] = I18n.t('shopping_carts.create')
    else
      flash[:error] = I18n.t('shopping_carts.error')
    end
    redirect_to products_path
  end

  private
  def find_cart
    @cart = ShoppingCart.find_by(user_id: current_user.id)
  end
end
