class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    # add custom create logic here
    @user = User.new(user_params)
    @user.role = Role.where(name: Constants::CLIENT).first

    respond_to do |format|
      if @user.save
        format.html {redirect_to :back, notice: 'user was successfully created.'}
        format.json {render :show, status: :created, location: @user}
      else
        format.html {render :new}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    super
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :role_id)
  end
end