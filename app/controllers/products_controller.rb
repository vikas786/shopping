class ProductsController < ApplicationController

  def index
    @products = if params[:search_term].present?
      Product.search(params[:search_term])
    else
      Product.all
    end
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = I18n.t('products.create')
      redirect_to products_path
    else
      flash[:error] = @product.errors.full_messages.first
      redirect_to new_product_path
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :item_type, :price, :brand_name, :serial_number)
  end
end
