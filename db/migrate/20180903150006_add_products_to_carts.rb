class AddProductsToCarts < ActiveRecord::Migration
  def change
    create_table :products_shopping_carts do |t|
      t.belongs_to :product,type: :uuid, index: true
      t.belongs_to :shopping_cart, index: true
      t.timestamps
    end
  end
end
  