class CreateProducts < ActiveRecord::Migration
  def change
    enable_extension "uuid-ossp"
    create_table :products,id: :uuid, default: 'uuid_generate_v4()', force: true do |t|
      t.string :name, index: true
      t.string :item_type, index: true
      t.string :price, index: true
      t.timestamps null: false
    end
  end
end
