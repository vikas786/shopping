class AddColumnsInProducts < ActiveRecord::Migration
  def change
    add_column :products, :brand_name, :string
    add_column :products, :serial_number, :string
  end
end
