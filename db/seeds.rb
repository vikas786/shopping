# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Role.create(name: "Admin")
Role.create(name: "Client")
User.create(name: "Admin User", email: "user@admin.com", role_id: Role.where(name: "Admin").first.id, password: "123456")
