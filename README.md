The Application live URL is:

https://test-shopping-cart.herokuapp.com/

# Simple Product and Cart App using Rails 4

The application allows Admin(Shop Owner) to create the Products and list all the products. The user can also search for the products by name, category, product brand or price.
The user can add the product into the cart.

The app uses Postgres as database.

### Prerequisites

This application needs the following versions of Ruby and Ruby on Rails

`Ruby >= 2.2.2`
`Rails >= 4.2.8`


### Installing

To setup this application, follow the below steps:

To install Gems run
`bundle install`

To create database run
`rake db:create`

To run migrations
`rake db:migrate`

To create default Admin user and roles
`rake db:seed`

You are good to test your app, execute
`rails server`


### Assumptions

There is a default Admin user with the below credentials which you can use to create products:

Email: `user@admin.com`
Password: `123456`

Before adding any product into cart, the user has to sign-up and login to the application.